package ru.inordic.ivasileva.calculator;

import java.util.Scanner;

public class Calculator {

    public void execute() {
        Scanner scanner = new Scanner(System.in);
        double result = 0.0;

        try {
            System.out.println("Insert the first number: ");
            double firstNumber = scanner.nextDouble();
            System.out.println("Insert the second number: ");
            double secondNumber = scanner.nextDouble();
            System.out.println(String.join("\r\n", "Select code operation (enter an integer number): ",
                    "0 - addition (+) ",
                    "1 - subtraction (-)",
                    "2 - multiplication (*)",
                    "3 - division (/)"));

            int selectOperation = Math.abs(scanner.nextInt());

            switch (selectOperation) {
                case 0:
                    result = firstNumber + secondNumber;
                    break;
                case 1:
                    result = firstNumber - secondNumber;
                    break;
                case 2:
                    result = firstNumber * selectOperation;
                    break;
                case 3:
                    result = firstNumber / secondNumber;
                    break;

                default:
                    System.out.println("You inserted incorrect operation code. Please, try again");
            }

            if (selectOperation <= 3) System.out.println("Your result is: " + result);

        } catch (RuntimeException ex) {
            System.out.println("Please, try again and enter the number");
        }

        scanner.close();
    }
}
