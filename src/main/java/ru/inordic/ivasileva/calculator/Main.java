package ru.inordic.ivasileva.calculator;

public class Main {
    public static void main(String[] args) {
        Calculator calculator = new Calculator();
        calculator.execute();
    }
}
